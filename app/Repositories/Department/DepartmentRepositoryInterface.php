<?php
namespace App\Repositories\Department;


interface DepartmentRepositoryInterface
{
    public function getDepartments($data);
}

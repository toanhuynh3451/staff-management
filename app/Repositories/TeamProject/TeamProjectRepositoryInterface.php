<?php
namespace App\Repositories\TeamProject;


interface TeamProjectRepositoryInterface
{
    public function getTeamProjects($data);
}

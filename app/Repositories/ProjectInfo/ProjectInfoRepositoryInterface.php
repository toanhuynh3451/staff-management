<?php
namespace App\Repositories\ProjectInfo;


interface ProjectInfoRepositoryInterface
{
    public function getProjects($data);
}
